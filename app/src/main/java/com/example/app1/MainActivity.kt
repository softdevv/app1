package com.example.app1

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class MainActivity : AppCompatActivity() {
//    data class Name (val txtName: String, val txtId: String)
    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var button = findViewById<Button>(R.id.button)
        button.setOnClickListener{
            val intent = Intent (this,HelloActivity::class.java)
            intent.putExtra("txtName","Nipitpon Pateepawanite")
            startActivity(intent)
            val name: TextView = findViewById(R.id.txtName)
            val id: TextView = findViewById(R.id.txtId)
            Log.d(TAG,""+name.text)
            Log.d(TAG,""+id.text)
    }
        supportActionBar!!.title= "HELLO"

    }
}