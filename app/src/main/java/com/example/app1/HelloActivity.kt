package com.example.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    var showname: TextView? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        showname =findViewById<TextView>(R.id.txtEdit)
        var intent = intent
        showname!!.text = intent.getStringExtra("txtName")

        supportActionBar!!.title= "HELLO"
    }
}